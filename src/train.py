import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.ensemble import RandomForestClassifier
import pickle

if __name__ == '__main__':
    X_train = pd.read_csv('data/prepared/X_train.csv', header=None)
    y_train = pd.read_csv('data/prepared/y_train.csv', header=None)

    classifier = RandomForestClassifier(n_estimators=10)
    # Applying classifier on training data
    classifier = classifier.fit(X_train, y_train)
    with open("model/model.pkl", "wb") as f:
        pickle.dump(classifier, f)

