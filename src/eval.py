import json
import pandas as pd
import pickle
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.metrics import ConfusionMatrixDisplay

if __name__ == '__main__':

    X_test = pd.read_csv('data/prepared/X_test.csv', header=None)
    y_test = pd.read_csv('data/prepared/y_test.csv', header=None)
    with open("model/model.pkl", "rb") as f:
        clf = pickle.load(f)
        prediction = clf.predict(X_test)
        accuracy = accuracy_score(y_test, prediction)
        precision = precision_score(y_test, prediction, average='macro')
        recall = recall_score(y_test, prediction, average='macro')
        f1 = f1_score(y_test, prediction, average='macro')
        print(accuracy)
        # check accuracy
        with open("prediction.txt", "w") as pred:
            pred.write(str(prediction))

        json.dump(
            obj={
                'accuracy_score': accuracy,
                'precision': precision,
                'recall': recall,
                'f1': f1
            },
            fp=open('metrics/accuracy.json', 'w')
        )
