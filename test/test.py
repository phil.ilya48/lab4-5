import pandas as pd
import pickle
from sklearn.metrics import accuracy_score
import pytest


def test():
    X_test = pd.read_csv('data/prepared/X_test.csv')
    y_test = pd.read_csv('data/prepared/y_test.csv')

    with open("model/model.pkl", "rb") as f:
        clf = pickle.load(f)
        prediction = clf.predict(X_test)

        # check accuracy
        accuracy = accuracy_score(y_test, prediction)
        assert accuracy > 0
