from sklearn import preprocessing
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split


if __name__ == '__main__':
    df = pd.read_csv('data/iris.data', header=None)
    headers = ["SepalLengthCm", "SepalWidthCm", "PetalLengthCm", "PetalWidthCm", "Species"]
    df.columns = headers

    label_encoder = preprocessing.LabelEncoder()
    df['Species'] = label_encoder.fit_transform(df['Species'])
    # Split into X and y
    X = df.drop(['Species'], axis=1)
    y = df['Species']

    # Apply standardScaler for X
    scaler = StandardScaler()
    X = scaler.fit_transform(X)

    # splitting data into training and test sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y, random_state=42)

    pd.DataFrame(X_train).to_csv('data/prepared/X_train.csv', header=False, index=False)
    pd.DataFrame(X_test).to_csv('data/prepared/X_test.csv', header=False, index=False)
    pd.DataFrame(y_train).to_csv('data/prepared/y_train.csv', header=False, index=False)
    pd.DataFrame(y_test).to_csv('data/prepared/y_test.csv', header=False, index=False)
